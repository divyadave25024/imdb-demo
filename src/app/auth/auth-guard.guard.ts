import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardGuard implements CanActivate {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let creds = localStorage.getItem('CREDS_PASS');
    if (creds == 'true') {
      localStorage.removeItem('CREDS_PASS');
      return true;
    } else {
      localStorage.removeItem('CREDS_PASS');
      return false;
    }
  }
}
