import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { USER_NAME, PWD } from '../user-creds.constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  userForm: FormGroup;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.userForm = new FormGroup({
      userName: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
    localStorage.removeItem('CREDS_PASS');
  }

  onSubmit() {
    this.userForm.markAsDirty();
    let formValue = this.userForm.controls;
    if (this.userForm.valid) {
      let username_success =
        this.userForm.controls.userName.value == USER_NAME ? true : false;
      let user_pwd =
        this.userForm.controls.password.value == PWD ? true : false;
      if (username_success && user_pwd) {
        this.fnRedirectDashboard();
      }
    }
  }

  fnRedirectDashboard() {
    localStorage.setItem('CREDS_PASS', 'true');
    this.router.navigate(['/dashboard']);
  }
}
