import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  title = 'IMDB';
  img_path = 'assets/images/movie_img.jpg';
  items: any = [
    {
      desc: 'movie description goes here',
      director: 'abcd',
      writer: 'Qwerty uiop',
      stars: 'Star names goes here',
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
